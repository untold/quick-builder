# About Quick Builder

Use Quick Builder to ease the build process by using custom presets and without the need of switching platform every time.

## Requirements

This version of the package is compatible with the following versions of the Unity Editor:

* 2021.0 and later (recommended)

## Package contents

The following table indicates the folder structure of the package:

|Location|Description|
|---|---|
|`<Editor>`|Editor folder. Contains all the code for the package's editor features.|
|`<Editor Default Resources>`|Editor folder. Contains all the assets used only by the editor.|

## Document revision history

|Date|Reason|
|---|---|
|January 19th, 2023|Document created. Matches package version 0.0.1|
