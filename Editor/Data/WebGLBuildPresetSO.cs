﻿using UnityEditor;
using UnityEngine;

namespace QuickBuilder.Editors.Data {
    [CreateAssetMenu(menuName = "Quick Build/Presets/WebGL", fileName = "New WebGL Build Preset", order = 0)]
    internal sealed class WebGLBuildPresetSO : BuildPresetSO {
        #region Public Variables
        #endregion

        #region Private Variables
        #endregion

        #region Properties
        protected override BuildTarget TargetPlatform => BuildTarget.WebGL;
        protected override BuildTargetGroup TargetGroup => BuildTargetGroup.WebGL;
        #endregion

        #region Behaviour Callbacks
        #endregion

        #region Public Methods
        #endregion

        #region Private Methods
        #endregion
    }
}