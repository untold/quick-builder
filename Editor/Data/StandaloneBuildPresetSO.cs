﻿using UnityEditor;
using UnityEngine;

namespace QuickBuilder.Editors.Data {
    [CreateAssetMenu(menuName = "Quick Build/Presets/Standalone", fileName = "New Standalone Build Preset", order = 0)]
    internal sealed class StandaloneBuildPresetSO : BuildPresetSO {
        private enum StandaloneBuildTarget { Windows, OSX, Linux }
        private enum StandaloneArchitecture { [InspectorName("Intel 32-bit")]Bit32, [InspectorName("Intel 64-bit")]Bit64 }

        #region Public Variables
        [field: SerializeField] private StandaloneBuildTarget Target { get; set; } = StandaloneBuildTarget.Windows;
        [field: SerializeField] private StandaloneArchitecture Architecture { get; set; } = StandaloneArchitecture.Bit64;
        #endregion

        #region Private Variables
        #endregion

        #region Properties
        protected override BuildTarget TargetPlatform => Target switch {
            StandaloneBuildTarget.Windows => Architecture switch {
                StandaloneArchitecture.Bit32 => BuildTarget.StandaloneWindows,
                _ => BuildTarget.StandaloneWindows64
            },
            StandaloneBuildTarget.OSX => BuildTarget.StandaloneOSX,
            _ => BuildTarget.StandaloneLinux64
        };
        protected override BuildTargetGroup TargetGroup => BuildTargetGroup.Standalone;
        #endregion

        #region Behaviour Callbacks
        #endregion

        #region Public Methods
        #endregion

        #region Private Methods
        #endregion
    }
}