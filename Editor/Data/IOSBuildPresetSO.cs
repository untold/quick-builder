﻿using UnityEditor;
using UnityEngine;

namespace QuickBuilder.Editors.Data {
    [CreateAssetMenu(menuName = "Quick Build/Presets/iOS", fileName = "New iOS Build Preset", order = 0)]
    internal sealed class IOSBuildPresetSO : BuildPresetSO {
        #region Public Variables
        #endregion

        #region Private Variables
        #endregion

        #region Properties
        protected override BuildTarget TargetPlatform => BuildTarget.iOS;
        protected override BuildTargetGroup TargetGroup => BuildTargetGroup.iOS;
        #endregion

        #region Behaviour Callbacks
        #endregion

        #region Public Methods
        #endregion

        #region Private Methods
        #endregion
    }
}