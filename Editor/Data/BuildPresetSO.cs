﻿using System.IO;
using System.Linq;
using UnityEditor;
using UnityEditor.Build.Reporting;
using UnityEngine;

namespace QuickBuilder.Editors.Data {
    public abstract class BuildPresetSO : ScriptableObject {
        private enum CompressionType { Default, LZ4, LZ4HC }

        #region Public Variables
        [field: SerializeField] protected string BuildPath { get; private set; } = null;

        [field: SerializeField] protected BuildOptions Options { get; private set; } = BuildOptions.None;

        [field: SerializeField] protected SceneAsset[] Scenes { get; set; }
        [field: SerializeField] protected string[] AdditionalDefineSymbols { get; set; }

        [field: SerializeField] protected bool IsDevelopment { get; private set; } = false;
        [field: SerializeField] protected bool AutoConnectProfiling { get; private set; } = false;
        [field: SerializeField] protected bool DeepProfiling { get; private set; } = false;
        [field: SerializeField] protected bool ScriptDebugging { get; private set; } = false;
        [field: SerializeField] private CompressionType Compression { get; set; } = CompressionType.Default;
        #endregion

        #region Private Variables
        #endregion

        #region Properties
        protected abstract BuildTarget TargetPlatform { get; }
        protected abstract BuildTargetGroup TargetGroup { get; }
        #endregion

        #region Behaviour Callbacks
        #endregion

        #region Public Methods
        public BuildTarget GetTargetPlatform() => TargetPlatform;
        public BuildTargetGroup GetTargetGroup() => TargetGroup;
        #endregion

        #region Private Methods
        private BuildPlayerOptions GetOptions() => new BuildPlayerOptions {
            locationPathName = BuildPath,
            target = TargetPlatform,
            targetGroup = TargetGroup,
            options = Options,
            scenes = Scenes.Select(AssetDatabase.GetAssetPath).ToArray(),
            extraScriptingDefines = AdditionalDefineSymbols,
        };

        internal void Build() {
            if (string.IsNullOrWhiteSpace(BuildPath)) {
                var useAutoPath = EditorUtility.DisplayDialog("Path not set", "The build path is not set, use the automatic one?", "Proceed", "Cancel");

                if (!useAutoPath)
                    return;

                BuildPath = Application.dataPath[..^("Assets/".Length)] + "/Builds/" + PlayerSettings.productName + " - " + TargetPlatform;
            }

            if (!Directory.Exists(BuildPath))
                Directory.CreateDirectory(BuildPath);

            var report = BuildPipeline.BuildPlayer(GetOptions());
            var summary = report.summary;

            switch (summary.result) {
                case BuildResult.Succeeded:
                    Debug.Log("Build succeeded: " + summary.totalSize + " bytes\n", this);
                    break;
                case BuildResult.Failed:
                    Debug.LogError("Build failed\n", this);
                    break;
                case BuildResult.Unknown:
                    Debug.LogError("Something prevented building the app\n", this);
                    break;
                case BuildResult.Cancelled:
                    Debug.LogWarning("Build was canceled\n", this);
                    break;
                default:
                    Debug.LogWarning("Build compiled with warnings\n", this);
                    break;
            }
        }
        #endregion
    }
}