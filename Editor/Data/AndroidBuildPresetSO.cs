﻿using UnityEditor;
using UnityEngine;

namespace QuickBuilder.Editors.Data {
    [CreateAssetMenu(menuName = "Quick Build/Presets/Android", fileName = "New Android Build Preset", order = 0)]
    internal sealed class AndroidBuildPresetSO : BuildPresetSO {
        #region Public Variables
        #endregion

        #region Private Variables
        #endregion

        #region Properties
        protected override BuildTarget TargetPlatform => BuildTarget.Android;
        protected override BuildTargetGroup TargetGroup => BuildTargetGroup.Android;
        #endregion

        #region Behaviour Callbacks
        #endregion

        #region Public Methods
        #endregion

        #region Private Methods
        #endregion
    }
}