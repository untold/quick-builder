﻿using UnityEditor;
using UnityEngine;

namespace QuickBuilder.Editors.Data {
    [System.Serializable]
    public sealed class SerializedBuildPlayerOptions {
        #region Public Variables
        [field: SerializeField] public string BuildPath { get; set; }

        [field: SerializeField, Space] public BuildTarget TargetPlatform { get; set; }
        [field: SerializeField] public BuildOptions Options { get; set; }

        [field: SerializeField, Space] public string[] Scenes { get; set; }
        [field: SerializeField] public string[] AdditionalDefineSymbols { get; set; }
        #endregion

        #region Private Variables
        #endregion

        #region Properties
        #endregion

        #region Behaviour Callbacks
        #endregion

        #region Public Methods
        public BuildPlayerOptions GetOptions() => new BuildPlayerOptions {
            locationPathName = BuildPath,
            target = TargetPlatform,
            options = Options,
            scenes = Scenes,
            extraScriptingDefines = AdditionalDefineSymbols,
        };
        #endregion

        #region Private Methods
        #endregion
    }
}