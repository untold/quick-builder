﻿/*using UnityEditor;
using UnityEditor.Build.Reporting;
using UnityEngine;

namespace QuickBuilder.Editors {
    internal static class CompilationUtils {
        #region Public Variables
        #endregion

        #region Private Variables
        #endregion

        #region Properties
        #endregion

        #region Constructors
        #endregion

        #region Public Methods
        [MenuItem("Build/Test/Build For Streamer")]
        public static void BuildAsStreamerTest() => BuildAsStreamer(false);
        [MenuItem("Build/Release/Build For Streamer")]
        public static void BuildAsStreamerRelease() => BuildAsStreamer(true);
        
        [MenuItem("Build/Test/Build For Audience")]
        public static void BuildAsAudienceTest() => BuildAsAudience(false);
        [MenuItem("Build/Release/Build For Audience")]
        public static void BuildAsAudienceRelease() => BuildAsAudience(true);
        #endregion

        #region Private Methods
        private static void BuildAsStreamer(bool isRelease) {
            var buildPath = EditorUtility.OpenFolderPanel("Build Path", Application.dataPath[..^("Assets/".Length)], "Builds");
            if (string.IsNullOrWhiteSpace(buildPath))
                return;
        
            var buildOptions = new BuildPlayerOptions {
                extraScriptingDefines = isRelease ? new []{ "STREAMER_BUILD", "RELEASE" } : new[]{ "STREAMER_BUILD" },
                scenes = new []{ @"Assets/Scenes/Agoras/Auditorium Eventi Streamer.unity" },
                locationPathName = buildPath,
                target = BuildTarget.WebGL,
                options = BuildOptions.None
            };
        
            var report = BuildPipeline.BuildPlayer(buildOptions);
            var summary = report.summary;
        
            switch (summary.result) {
                case BuildResult.Succeeded:
                    Debug.Log("Build succeeded: " + summary.totalSize + " bytes\n");
                    break;
                case BuildResult.Failed:
                    Debug.LogError("Build failed\n");
                    break;
                default:
                    Debug.LogWarning("Build compiled with warnings\n");
                    break;
            }
        }
        
        private static void BuildAsAudience(bool isRelease) {
            var buildPath = EditorUtility.OpenFolderPanel("Build Path", Application.dataPath[..^("Assets/".Length)], "Builds");
            if (string.IsNullOrWhiteSpace(buildPath))
                return;
        
            var buildOptions = new BuildPlayerOptions {
                extraScriptingDefines = isRelease ? new []{ "AUDIENCE_BUILD", "RELEASE" } : new[]{ "AUDIENCE_BUILD" },
                scenes = new []{ @"Assets/Scenes/VaffaSpacestation.unity", @"Assets/Scenes/Agoras/Auditorium Eventi.unity" },
                locationPathName = buildPath,
                target = BuildTarget.WebGL,
                options = BuildOptions.None
            };
        
            var report = BuildPipeline.BuildPlayer(buildOptions);
            var summary = report.summary;
        
            switch (summary.result) {
                case BuildResult.Succeeded:
                    Debug.Log("Build succeeded: " + summary.totalSize + " bytes\n");
                    break;
                case BuildResult.Failed:
                    Debug.LogError("Build failed\n");
                    break;
                default:
                    Debug.LogWarning("Build compiled with warnings\n");
                    break;
            }
        }
        #endregion
    }
}*/