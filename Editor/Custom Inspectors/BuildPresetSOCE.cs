﻿using System.Reflection;
using QuickBuilder.Editors.Data;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace QuickBuilder.Editors.CustomInspectors {
    [CanEditMultipleObjects]
    [CustomEditor(typeof(BuildPresetSO), true)]
    internal class BuildPresetSOCE : Editor {
        #region Public Variables
        #endregion

        #region Private Variables
        private SerializedProperty _buildPathProp;
        private SerializedProperty _optionsProp;
        private SerializedProperty _scenesProp;
        private SerializedProperty _definesProp;

        private ReorderableList _scenesList;
        private bool _lockScenesList = true;

        private ReorderableList _definesList;
        private bool _lockDefinesList = true;
        #endregion

        #region Properties
        private static string _placeholderPath;
        private System.Type _moduleManager;
        private MethodInfo _isPlatformSupportLoaded;
        private MethodInfo _getTargetStringFromBuildTarget;
        #endregion

        #region Editor Callbacks
        private void OnEnable() {
            _placeholderPath = Application.dataPath[..^("Assets/".Length)];

            _buildPathProp = serializedObject.FindProperty("<BuildPath>k__BackingField");
            _optionsProp = serializedObject.FindProperty("<Options>k__BackingField");
            _scenesProp = serializedObject.FindProperty("<Scenes>k__BackingField");
            _definesProp = serializedObject.FindProperty("<AdditionalDefineSymbols>k__BackingField");

            OnPrepare();

            _scenesList = new ReorderableList(serializedObject, _scenesProp, true, true, true, true) {
                drawHeaderCallback = Scenes_DrawHeader,
                drawElementCallback = Scenes_DrawElement,
                drawNoneElementCallback = Scenes_DrawNoElements,
                onCanAddCallback = Scenes_CanEdit,
                onCanRemoveCallback = Scenes_CanEdit,
                onAddCallback = Scenes_AddElement,
                onRemoveCallback = Scenes_RemoveElement,
                onReorderCallbackWithDetails = Scenes_Reorder,
            };

            _definesList = new ReorderableList(serializedObject, _definesProp, true, true, true, true) {
                drawHeaderCallback = Defines_DrawHeader,
                drawElementCallback = Defines_DrawElement,
                drawNoneElementCallback = Defines_DrawNoElements,
                onCanAddCallback = Defines_CanEdit,
                onCanRemoveCallback = Defines_CanEdit,
                onAddCallback = Defines_AddElement,
                onRemoveCallback = Defines_RemoveElement,
                onReorderCallbackWithDetails = Defines_Reorder,
            };
        }

        public override void OnInspectorGUI() {
            serializedObject.Update();

            DrawPathField();

            EditorGUILayout.Space();
            EditorGUILayout.PropertyField(_optionsProp);

            OnDrawInspector();

            EditorGUILayout.Space();
            _scenesList.draggable = !_lockScenesList;
            _scenesList.DoLayoutList();

            EditorGUILayout.Space();
            _definesList.draggable = !_lockDefinesList;
            _definesList.DoLayoutList();

            serializedObject.ApplyModifiedProperties();

            var canBuild = CheckPlatformIsInstalled();

            if (canBuild)
                DrawBuildButton();
        }

        private void DrawPathField() {
            var originalColor = GUI.color;

            GUILayout.BeginHorizontal();
            {
                EditorGUILayout.PropertyField(_buildPathProp);
                if (string.IsNullOrEmpty(_buildPathProp.stringValue)) {
                    var r = GUILayoutUtility.GetLastRect();
                    r.xMin += EditorGUIUtility.labelWidth + 4;
                    GUI.color = Color.gray;
                    EditorGUI.LabelField(r, _placeholderPath);
                    GUI.color = originalColor;
                }

                GUILayout.Space(-2);
                if (GUILayout.Button(EditorGUIUtility.IconContent("d_FolderOpened Icon"), EditorStyles.miniButtonRight, GUILayout.Width(24))) {
                    GUILayout.EndHorizontal();
                    var buildPath = EditorUtility.OpenFolderPanel("Build Path", Application.dataPath[..^("Assets/".Length)], "Builds");
                    if (!string.IsNullOrWhiteSpace(buildPath))
                        _buildPathProp.stringValue = buildPath;
                }
            }
            GUILayout.EndHorizontal();
        }

        private bool CheckPlatformIsInstalled() {
            var canBuild = CanBuild();

            if (canBuild)
                return true;

            EditorGUILayout.HelpBox("The target platform is not installed!", MessageType.Error);
            return false;
        }

        private void DrawBuildButton() {
            GUI.enabled = _scenesProp.arraySize > 0;

            if (GUILayout.Button("Build"))
                (serializedObject.targetObject as BuildPresetSO)!.Build();

            GUI.enabled = true;
        }
        #endregion

        #region Public Methods
        #endregion

        #region Private Methods
        protected virtual void OnPrepare() {}
        protected virtual void OnDrawInspector() {}

        private bool CanBuild() {
            var targetPlatform = (serializedObject.targetObject as BuildPresetSO)!.GetTargetPlatform();
            var targetGroup = (serializedObject.targetObject as BuildPresetSO)!.GetTargetGroup();
            return BuildPipeline.IsBuildTargetSupported(targetGroup, targetPlatform);

            // var targetPlatform = (serializedObject.targetObject as BuildPresetSO)!.GetTargetPlatform();
            //
            // _moduleManager ??= System.Type.GetType("UnityEditor.Modules.ModuleManager,UnityEditor.dll");
            // _isPlatformSupportLoaded = _moduleManager!.GetMethod(
            //     "IsPlatformSupportLoaded",
            //     BindingFlags.Static | BindingFlags.NonPublic);
            // _getTargetStringFromBuildTarget = _moduleManager.GetMethod(
            //     "GetTargetStringFromBuildTarget",
            //     BindingFlags.Static | BindingFlags.NonPublic);
            //
            // return (bool)_isPlatformSupportLoaded!.Invoke(
            //     null,
            //     new object[] { (string)_getTargetStringFromBuildTarget!.Invoke(null, new object[] { targetPlatform }) }
            // );
        }
        #endregion

        #region Event Methods
        private void Scenes_DrawHeader(Rect rect) {
            GUI.Label(rect, "Scenes", EditorStyles.boldLabel);

            var lockToggleRect = new Rect(rect) {
                x = rect.xMax - 12,
                width = 21
            };
            _lockScenesList = GUI.Toggle(lockToggleRect, _lockScenesList, EditorGUIUtility.IconContent(_lockScenesList ? "LockIcon-On" : "LockIcon"), EditorStyles.label);
        }

        private void Scenes_DrawNoElements(Rect rect) => GUI.Label(rect, new GUIContent(" No scenes are set, at least one scene must be present", EditorGUIUtility.IconContent("console.erroricon.sml").image), EditorStyles.centeredGreyMiniLabel);

        private void Scenes_DrawElement(Rect rect, int index, bool isActive, bool isFocused) {
            var ithElement = _scenesProp.GetArrayElementAtIndex(index);
            var r = new Rect(rect) {
                y = rect.y + 2,
                height = EditorGUIUtility.singleLineHeight
            };

            GUI.enabled = !_lockScenesList;
            EditorGUI.PropertyField(r, ithElement, GUIContent.none);
            GUI.enabled = true;
        }

        private bool Scenes_CanEdit(ReorderableList list) => !_lockScenesList;

        private void Scenes_AddElement(ReorderableList list) {
            serializedObject.Update();
            _scenesProp.InsertArrayElementAtIndex(list.count);
            serializedObject.ApplyModifiedProperties();
        }
        private void Scenes_RemoveElement(ReorderableList list) {
            serializedObject.Update();
            _scenesProp.DeleteArrayElementAtIndex(list.index);
            serializedObject.ApplyModifiedProperties();
        }

        private void Scenes_Reorder(ReorderableList list, int oldIndex, int newIndex) {
            serializedObject.Update();
            _scenesProp.MoveArrayElement(oldIndex, newIndex);
            serializedObject.ApplyModifiedProperties();
        }

        // -----------

        private void Defines_DrawHeader(Rect rect) {
            GUI.Label(rect, "Additional Defines Symbols", EditorStyles.boldLabel);

            var lockToggleRect = new Rect(rect) {
                x = rect.xMax - 12,
                width = 21
            };
            _lockDefinesList = GUI.Toggle(lockToggleRect, _lockDefinesList, EditorGUIUtility.IconContent(_lockDefinesList ? "LockIcon-On" : "LockIcon"), EditorStyles.label);
        }

        private void Defines_DrawNoElements(Rect rect) => GUI.Label(rect, "No additional defines are set", EditorStyles.centeredGreyMiniLabel);

        private void Defines_DrawElement(Rect rect, int index, bool isActive, bool isFocused) {
            var ithElement = _definesProp.GetArrayElementAtIndex(index);
            var r = new Rect(rect) {
                y = rect.y + 2,
                height = EditorGUIUtility.singleLineHeight
            };

            GUI.enabled = !_lockDefinesList;
            EditorGUI.PropertyField(r, ithElement, GUIContent.none);
            GUI.enabled = true;
        }

        private bool Defines_CanEdit(ReorderableList list) => !_lockDefinesList;

        private void Defines_AddElement(ReorderableList list) {
            serializedObject.Update();
            _definesProp.InsertArrayElementAtIndex(list.count);
            serializedObject.ApplyModifiedProperties();
        }
        private void Defines_RemoveElement(ReorderableList list) {
            serializedObject.Update();
            _definesProp.DeleteArrayElementAtIndex(list.index);
            serializedObject.ApplyModifiedProperties();
        }

        private void Defines_Reorder(ReorderableList list, int oldIndex, int newIndex) {
            serializedObject.Update();
            _definesProp.MoveArrayElement(oldIndex, newIndex);
            serializedObject.ApplyModifiedProperties();
        }
        #endregion
    }
}