﻿using QuickBuilder.Editors.Data;
using UnityEditor;

namespace QuickBuilder.Editors.CustomInspectors {
    [CanEditMultipleObjects]
    [CustomEditor(typeof(StandaloneBuildPresetSO))]
    internal class StandaloneBuildPresetSOCE : BuildPresetSOCE {
        #region Private Variables
        private SerializedProperty _targetProp;
        private SerializedProperty _architectureProp;
        #endregion

        #region Overrides
        protected override void OnPrepare() {
            _targetProp = serializedObject.FindProperty("<Target>k__BackingField");
            _architectureProp = serializedObject.FindProperty("<Architecture>k__BackingField");
        }

        protected override void OnDrawInspector() {
            EditorGUILayout.PropertyField(_targetProp);
            if (_targetProp.enumValueIndex == 0)
                EditorGUILayout.PropertyField(_architectureProp);
        }
        #endregion
    }
}