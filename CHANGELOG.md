# Changelog
All notable changes to this package will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.2/).

## [0.0.1] - 2023-01-19
This is the first setup of the <b>Quick Builder</b> Package.

## [0.1.0] - 2023-01-19
Fixed minor issues with the Inspectors.